_Accounting using command-line_ (accli) is a simple and extensible
framework and toolkit for business managing for small companies and/or
personal projects. It is written in Python and released under GPLv3.


## Installation

On GNU/Linux:

```
$ apt-get install libpython3-dev virtualenv
$ virtualenv --python=/usr/bin/python3 accli
$ source accli/bin/activate
(accli) $ pip install -r requirements.txt
```
